package Ejercicio1;

import java.io.*;
import java.util.*;

public class Ejercicio4 {

    public static void main(String[] args) throws IOException {
        System.out.println("stop para salir");
        caso1(); //acabado
        //caso2();// no va
        //caso3();//no va
    }

    public static void caso1() throws IOException{
        //llama al .jar
        String command = "java -jar out\\artifacts\\Traductor_jar\\Practica3.jar";
        List<String> argList = new ArrayList<>(Arrays.asList(command.split(" ")));
        //inicia los procesos
        ProcessBuilder pb = new ProcessBuilder(argList);
        Process process = pb.start();
        //comunica con el hijo.
        OutputStream os = process.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os);
        BufferedWriter bw = new BufferedWriter(osw);
        Scanner procesoSC = new Scanner(process.getInputStream());
        //escaner para preguntar la palabra a traducir.
        Scanner sc = new Scanner(System.in);
        String p = sc.nextLine();
        String palabra;
        try{
            //while para cerrar el programa.
            while(!p.equals("stop")){
                //se envia la palabra al hijo.
                bw.write(p);
                bw.newLine();
                bw.flush();
                //resultado qque devuelve el hijo.
                palabra = procesoSC.nextLine();
                System.out.println(palabra);
                //escribe la palabra en Translations.txt
                escribir(palabra, p);
                //pide otra palabra.
                p = sc.nextLine();
            }
            bw.close();
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    //no se por que ocurre el error
    public static void caso2() throws IOException {
        String command = "java -jar out\\artifacts\\Tc2_jar\\Practica3.jar";
        List<String> argList = new ArrayList<>(Arrays.asList(command.split(" ")));
        ProcessBuilder pb = new ProcessBuilder(argList);
        Process process = pb.start();

        OutputStream os = process.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os);
        BufferedWriter bw = new BufferedWriter(osw);
        Scanner procesoSC = new Scanner(process.getInputStream());

        Scanner sc = new Scanner(System.in);
        String p = sc.nextLine();
        try{
            while(!p.equals("stop")){
                bw.write(p);
                bw.newLine();
                bw.flush();
                System.out.println(procesoSC.nextLine());
                p = sc.nextLine();
            }
            bw.close();
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
    //no e podido acabarlo
    public  static  void caso3() throws IOException {
        String command = "java -jar out\\artifacts\\Tc3_jar\\Practica3.jar";
        List<String> argList = new ArrayList<>(Arrays.asList(command.split(" ")));
        ProcessBuilder pb = new ProcessBuilder(argList);
        Process process = pb.start();

        pb.inheritIO();
        Scanner procesoSC = new Scanner(process.getInputStream());
        String p = procesoSC.nextLine();
        System.out.println(p);
    }
    //no e podido acabarlo
    public static void caso4() throws IOException {
        String command = "java -jar out\\artifacts\\Tc3_jar\\Practica3.jar";
        List<String> argList = new ArrayList<>(Arrays.asList(command.split(" ")));
        ProcessBuilder pb = new ProcessBuilder(argList);
        Process process = pb.start();
    }

    public static void escribir(String palabraTraducida, String palabra) throws IOException {
        //if para comprobar que las palabras se conozcan.
        if(!palabra.equals("stop")){
            if(!palabraTraducida.equals("Desconocido")){
                //Escribe en el fichero.
                BufferedWriter bw = new BufferedWriter(new FileWriter("Translations.txt", true));
                //formato.
                bw.write("<" + palabra + ">" +" - " + ">> " + "<" + palabraTraducida + ">");
                //salto de linea.
                bw.newLine();
                bw.close();
            }
        }
    }
}
