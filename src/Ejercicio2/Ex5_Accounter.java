package Ejercicio2;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Ex5_Accounter {
    private static ArrayList<Integer> sumaTotal = new ArrayList<Integer>();
    public static void main(String[] args) throws IOException {
        //Varible para sumar los archivos.
        int suma = 0;
        //buffer para escribir la suma de todos los ficheros.
        BufferedWriter bw1 = new BufferedWriter(new FileWriter("Totals.txt"));

        //bucle para sumar los ficheros por separado.
        for(int i = 0; i < args.length; i++){
            nuevoFichero(args[i]);
        }

        //bucle para sumar el valor de todos los ficheros  juntos.
        for(int num : sumaTotal){
            suma += num;
            bw1.write(String.valueOf(num));
            bw1.newLine();
        }

        //mensages para el usuario.
        bw1.write("La suma total de todos los archivos es: " + suma);
        System.out.println("La suma total de todos los archivos es: " + suma);
        System.out.println("Los resultados se han guardado en Totals.txt");
        bw1.close();
    }

    public static void nuevoFichero(String fichero)  {
        //parametros con el fichero que se pasa al hijo para que lo sume.
        String command = "java -jar out\\artifacts\\Adder_jar\\Practica3.jar " + fichero;
        List<String> argList = new ArrayList<>(Arrays.asList(command.split(" ")));
        //comunicacion conel hijo
        try {
            ProcessBuilder pb = new ProcessBuilder(argList);
            Process process = pb.start();
            pb.inheritIO();
            Scanner procesoSC = new Scanner(process.getInputStream());
            //el resultado que devuelve el hijo se pasa a una arrayList.
            sumaTotal.add(Integer.valueOf(procesoSC.nextLine()));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
