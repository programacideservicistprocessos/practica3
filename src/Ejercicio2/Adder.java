package Ejercicio2;

import java.io.*;

public class Adder {
    public static void main(String[] args) throws IOException {
        //fichero que pasa el padre.
        String fichero = args[0];
        //buffer para leer el ficero.
        BufferedReader br = new BufferedReader(new FileReader(fichero));
        //variable para ver liania a liania el fichero.
        String i;
        int suma = 0;
        //suma de numeros dentro del fichero.
        while((i = (br.readLine())) != null){
            suma += Integer.valueOf(i);
        }
        //impreime el resultado que se pasa al padre.
        System.out.println(suma);
    }
}
