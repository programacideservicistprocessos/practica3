package Ejercicio1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

public class TraductorCaso2 {
    private static String fichero;
    private static HashMap hm = new HashMap();
    public static void main(String[] args) throws IOException {
    fichero = args[1];

        Scanner sc = new Scanner(System.in);
        String p = sc.nextLine();
        while (!p.equals("stop")){
            buscarTraduccion(p);
            p = sc.nextLine();
        }
    }
    public static void buscarTraduccion(String palabra ) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fichero));
        String i;
        String[] partI = new String[2];

        while((i = (br.readLine())) != null){
             partI= i.split(" ");
             volverHashMap(partI[0], partI[1]);
            if(hm.containsKey(palabra)){
                 System.out.println(hm.get(palabra));
            }else{
                 System.out.println("Desconocido");
            }
        }
    }

    public static void volverHashMap(String key, String valor){
        hm.put(key, valor);
    }
}
