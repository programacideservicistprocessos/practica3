package Ejercicio1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class TraductorCaso3 {
    private static String fichero;
    private static HashMap hm = new HashMap();
    public static void main(String[] args) throws IOException {
        volverHashMap();
        fichero = args[0];
        buscarTraduccion();
    }

    public static void buscarTraduccion( ) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fichero));
        String i;

        while((i = (br.readLine())) != null){
            if(hm.containsKey(i)){
                System.out.println(hm.get(i));
            }else{
                System.out.println("Desconocido");
            }
        }
    }

    public static void volverHashMap(){
        //inicia el hashMap.
        hm.put("hello", "hola");
        hm.put("blue", "azul");
    }
}
