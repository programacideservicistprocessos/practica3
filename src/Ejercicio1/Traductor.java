package Ejercicio1;

import java.util.HashMap;
import java.util.Scanner;

public class Traductor {
    private static HashMap hm = new HashMap();
    public static void main(String[] args) {
        //inicia el hashMap.
        volverHashMap();
        Scanner sc = new Scanner(System.in);
        String p = sc.nextLine();
        while (!p.equals("stop")){
            //buscar la traduccion.
            buscarTraduccion(p);
            p = sc.nextLine();
        }
    }
    public static void buscarTraduccion(String palabra ){
        //if para buscar la traduccion correcta.
        if(hm.containsKey(palabra)){
            //imprime la traduccion.
            System.out.println(hm.get(palabra.toLowerCase()));
        }else{
            System.out.println("Desconocido");
        }
    }

    public static void volverHashMap(){
        //inicia el hashMap.
        hm.put("hello", "hola");
        hm.put("blue", "azul");
    }
}
